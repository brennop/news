const darkBtn = document.getElementById("dark-switch");
let isDark = false;
darkBtn.onclick = () => {
  isDark = !isDark;
  document.body.style.background = isDark
    ? "var(--color-bg-dark)"
    : "var(--color-bg-light)";

  /* dark btn icon */
  darkBtn.classList.toggle("fas");
  darkBtn.classList.toggle("far");

  /* header */
  const header = document.getElementById("header");
  if (header.classList.contains("dark")) {
    header.style.color = "var(--color-text-dark)";
    header.style.background = "var(--color-header-light)";
    header.classList.remove("dark");
  } else {
    header.style.color = "var(--color-text-light)";
    header.style.background = "var(--color-header-dark)";
    header.classList.add("dark");
  }

  const cards = document.querySelectorAll(".news-card, .sidebar");
  cards.forEach(card => {
    if (card.classList.contains("dark")) {
      card.style.color = "var(--color-text-dark)";
      card.style.background = "var(--color-card-light)";
      card.classList.remove("dark");
    } else {
      card.style.color = "var(--color-text-light)";
      card.style.background = "var(--color-card-dark)";
      card.classList.add("dark");
    }
  });
};
